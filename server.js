import { ApolloServer } from "@apollo/server";
import { userData ,clientDescription} from "./fakeDB.js";
import { expressMiddleware } from '@apollo/server/express4';    
import { PubSub } from "graphql-subscriptions";
import {
  ApolloServerPluginDrainHttpServer,
  ApolloServerPluginLandingPageDisabled,
  ApolloServerPluginLandingPageGraphQLPlayground
} from "apollo-server-core";
import express from 'express';
import { createServer } from 'http';
import { makeExecutableSchema } from '@graphql-tools/schema';
import { WebSocketServer } from 'ws';
import { useServer } from 'graphql-ws/lib/use/ws';
import bodyParser from 'body-parser';
import cors from 'cors';
import dotenv from 'dotenv';




const PORT = process.env.PORT || 4080;
const pubsub = new PubSub();

if (process.env.NODE_ENV !== "production") {
  dotenv.config()
}

const typeDefs = `#graphql
type Query {
    userData:[user]
    user(id:ID!):user
    getIdByClient(id:ID!):[client]
    clientDescription:[client]
}

type user{
    id:ID
    FirstName:String
    lastName:String
    email:String
    password:String
}

type client{
    id:ID
    description:String
}

type Mutation {
    signupUser( FirstName:String!, lastName:String!, email:String!, password:String!):user
    clientAdd(newClient:clientInput!): client

}

input clientInput{
    id:ID!
    description:String!
}

type Message {
    id: ID!
    content: String!
  }

  type Query {
    messages: [Message!]!
  }

  type Mutation {
    postMessage(content: String!): ID!
  }

  type Subscription {
    newMessage: Message!
  }

  type Subscription {
    newSignUpUser: user!
  }
`

// In-memory data store for simplicity
const messages = [];

const resolvers = {
    Query: {
      userData: () => userData,
      user:(_,arg)=> userData.find((e)=> e.id === arg.id ),
      getIdByClient:(_,{id})=> clientDescription.filter(e=> e.id ===id),
      clientDescription: () => clientDescription,
        messages: () => messages,
    },

    Mutation:{
        signupUser:(_,{ FirstName,lastName,email,password}) =>{
           let  id = Math.floor((Math.random() * 100) + 1)
            userData.push({
                id,
                FirstName,lastName,email,password
            })

            pubsub.publish('NEW_UserUp', { newSignUpUser: userData });
            
            console.log("userData", userData);
            return userData.find(e => e.id === id)
        },
        clientAdd:(_,{newClient}) =>{
            let  id = Math.floor((Math.random() * 100) + 1)
            clientDescription.push({
                id,
                ...newClient
            })
            return clientDescription.find(e => e.id === id)  
        },
        postMessage: (_, { content }) => {
            const id = messages.length.toString();
            const message = { id, content };
            messages.push(message);    
            // Publish the new message to the subscription channel
            pubsub.publish('NEW_MESSAGE', { newMessage: message });
            return id;
        },
    },
     // setting up subscription
    Subscription: {
        newMessage: {
            subscribe: () => pubsub.asyncIterator(['NEW_MESSAGE']),
        },
        newSignUpUser: {
            subscribe: () => pubsub.asyncIterator(['NEW_UserUp']),
        },
    },
  };

  // the WebSocket server.
const schema = makeExecutableSchema({ typeDefs, resolvers });

// server and the ApolloServer to this HTTP server.
const app = express();
app.use(cors());
const httpServer = createServer(app);

app.get("/",(req,res) =>{
  res.send("Hello Live 🤣😂 ")
})

// Set up WebSocket server.
const wsServer = new WebSocketServer({
  server: httpServer,
  path: '/graphql',
});
const serverCleanup = useServer({ schema }, wsServer);

// Set up ApolloServer.
const apolloServer = new ApolloServer({
  schema,
  plugins: [
    // Proper shutdown for the HTTP server.
    ApolloServerPluginDrainHttpServer({ httpServer }),
  
    // Proper shutdown for the WebSocket server.
    {
      async serverWillStart() {
        return {
          async drainServer() {
            await serverCleanup.dispose();
          },
        };
      },
    },
    process.env.NODE_ENV !== "production" ? ApolloServerPluginLandingPageGraphQLPlayground() :ApolloServerPluginLandingPageDisabled()
  ],
});

(async function () {
    // starting the apollo server to expose endoint to client
    await apolloServer.start();
    // apolloServer.applyMiddleware({
    //   app,path:'/graphql'
    // })
    app.use("/graphql", bodyParser.json(), expressMiddleware(apolloServer));
  })();

// Now that our HTTP server is fully set up, actually listen.
httpServer.listen(PORT, () => {
  console.log(`🚀 Query endpoint ready at http://localhost:${PORT}/graphql`);
  console.log(`🚀 Subscription endpoint ready at ws://localhost:${PORT}/graphql`);
});

